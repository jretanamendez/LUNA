$(function() {
  var flame = $('#flame');
  var txt = $('h1');

  flame.on({
    click: function() {
      flame.removeClass('burn').addClass('puff');
      $('.smoke').each(function() {
        $(this).addClass('puff-bubble');
      });
      $('#glow').remove();
      txt.html(" <b>Feliz <b> <b>Cumpleaños</b> niña...").delay(2750).fadeOut(300);
      $('#candle').animate({
        //'opacity': '.5'
      }, 100);

      // Crear el contexto de audio y reproducir el sonido dentro del evento de clic
      var ctx = new AudioContext();
      createSound(ctx, 20, 5000, 1, "sawtooth", 1); // Ajusta los parámetros según tus necesidades
    }
  });

  // Manejador de eventos para tocar la pantalla en cualquier lugar
  document.addEventListener("touchstart", function() {
    // Crear el contexto de audio y reproducir el sonido dentro del evento de toque
    var ctx = new AudioContext();
    createSound(ctx, 20, 5000, 1, "sawtooth", 1); // Ajusta los parámetros según tus necesidades
  });

  // Función para crear el sonido
  function createSound(ctx, size, fr, delay, type, vol) {
    for (let i = 0; i < size; i++) {
      let osc = ctx.createOscillator(),
          gain = ctx.createGain();

      setTimeout(function() {
        osc.frequency.value = fr * i;
        gain.gain.value = vol;
        osc.type = type;
        osc.connect(gain);
        gain.connect(ctx.destination);
        osc.start();
        setTimeout(function() {
          let gVal = gain.gain.value;
          let smooth;

          function reduceGain() {
            gVal -= 0.02;
            if (gVal > 0) {
              smooth = requestAnimationFrame(reduceGain);
            } else {
              osc.stop();
              cancelAnimationFrame(smooth);
            }

            gain.gain.value = gVal / 7;
          }
          reduceGain();

        }, delay);

      }, i * delay);
    }
  }

  // Redirección al hacer clic en el botón "Centro"
  $('#centerButton').click(function() {
    window.location.href = 'indexcarta.html';
  });
// Redirección al hacer clic en el botón "Izquierdo"
$('.btn-left').click(function() {
  window.open('https://64.media.tumblr.com/0c1f48e75f91bb7696314470667ed4ad/c113ada6af61c8b9-8d/s1280x1920/e4795fa88777e50ebd03c82cbedc993c8fbaffbe.png', '_blank');
});

// Redirección al hacer clic en el botón "Derecho"
$('.btn-right').click(function() {
  window.open('https://somoskudasai.com/wp-content/uploads/2021/12/FF749-jXsAMPah7.jpg', '_blank');
});


  // Mostrar el botón "Centro" después de 5 segundos
  setTimeout(function() {
    $('#centerButton').fadeIn();
  }, 5000); // 5000 milisegundos = 5 segundos
});
